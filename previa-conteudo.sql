-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23-Jul-2015 às 16:37
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `orienta`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atuacao`
--

CREATE TABLE IF NOT EXISTS `atuacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `atuacao_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `atuacao`
--

INSERT INTO `atuacao` (`id`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Planejamento', 'planejamento', '<p>Defini&ccedil;&otilde;es de Estrat&eacute;gias para Implanta&ccedil;&atilde;o de Projetos</p>\r\n\r\n<p>Viabilidade Econ&ocirc;mica e Ecol&oacute;gica</p>\r\n\r\n<p>An&aacute;lise de Riscos e Custos Ambientais</p>\r\n\r\n<p>Avalia&ccedil;&atilde;o de Passivos Ambientais e Auditoria Due Diligence</p>\r\n\r\n<p>Avalia&ccedil;&atilde;o de Impacto Ambiental (AIA) e Ambiental Estrat&eacute;gica (AAE)</p>\r\n\r\n<p>Defini&ccedil;&atilde;o de Indicadores Socioambientais e Sustentabilidade</p>\r\n\r\n<p>Interface com Engenharia para otimiza&ccedil;&atilde;o de projetos</p>\r\n', '0000-00-00 00:00:00', '2015-07-23 17:18:24'),
(2, 'Execução', 'execucao', '<p>Gest&atilde;o Ambiental de empreendimentos</p>\r\n\r\n<p>Obten&ccedil;&atilde;o de Licen&ccedil;as Ambientais e autoriza&ccedil;&otilde;es espec&iacute;ficas</p>\r\n\r\n<p>Lideran&ccedil;a de Estudos Multidisciplinares</p>\r\n\r\n<p>Coordena&ccedil;&atilde;o de estudos, programas e diagn&oacute;sticos socioambientais</p>\r\n\r\n<p>Elabora&ccedil;&atilde;o de estudos pontuais do meio bi&oacute;tico, f&iacute;sico, socioecon&ocirc;mico e multidisciplinares</p>\r\n\r\n<p>Interface com &oacute;rg&atilde;os ambientais e intervenientes</p>\r\n\r\n<p>Integra&ccedil;&atilde;o e consolida&ccedil;&atilde;o de dados e resultados de diferentes programas</p>\r\n\r\n<p>Supervis&atilde;o Ambiental das Obras</p>\r\n', '0000-00-00 00:00:00', '2015-07-23 17:17:54'),
(3, 'Monitoramento e Controle', 'monitoramento-e-controle', '<p>Implanta&ccedil;&atilde;o do Sistema de Gest&atilde;o Ambiental</p>\r\n\r\n<p>Verifica&ccedil;&atilde;o do atendimento padr&otilde;es do Internacional Finance Corporation</p>\r\n\r\n<p>Controle do atendimento a condicionantes e dos programas</p>\r\n\r\n<p>Auditoria em Sa&uacute;de, Seguran&ccedil;a do Trabalho e Meio Ambiente</p>\r\n\r\n<p>Treinamentos e Palestras</p>\r\n\r\n<p>Forma&ccedil;&atilde;o de equipe t&eacute;cnica e operacional</p>\r\n\r\n<p>Atendimento da Legisla&ccedil;&atilde;o Ambiental e Outros Requisitos Aplic&aacute;veis</p>\r\n', '0000-00-00 00:00:00', '2015-07-23 17:16:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, '20150723143408_slide3.jpg', '<p><span style="font-size:40px"><span style="font-size:70px"><strong>Simplifique</strong></span> e <span style="font-size:70px"><strong>Transforme</strong></span>:<br />\r\nn&atilde;o permita que o Meio Ambiente cause<br />\r\nimpacto ao seu projeto.</span></p>\r\n', '2015-07-23 17:34:08', '2015-07-23 17:34:08'),
(2, 0, '20150723143547_slide2.jpg', '<p><span style="font-size:40px">&ldquo;Para a humanidade <strong><span style="font-size:60px">sobreviver</span></strong>,<br />\r\nprecisaremos de uma maneira<br />\r\nsubstancialmente nova de <strong><span style="font-size:60px">pensar</span></strong>.&rdquo;</span></p>\r\n\r\n<p style="text-align: right;"><span style="font-size:30px">Albert Einstein</span></p>\r\n', '2015-07-23 17:35:48', '2015-07-23 17:35:48'),
(3, 0, '20150723143648_slide1.jpg', '<p><span style="font-size:40px"><span style="font-size:80px"><strong>Inove</strong></span> e <strong><span style="font-size:80px">mude</span></strong> seus h&aacute;bitos,<br />\r\nabra espa&ccedil;o para o novo.</span></p>\r\n', '2015-07-23 17:36:49', '2015-07-23 17:36:49');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `ordem`, `marca`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, '20150723142114_teles.jpg', 'Companhia Hidrelétrica Teles Pires', '<p>A Usina Hidrel&eacute;trica Teles Pires est&aacute; instalada no rio Teles Pires na divisa dos estados do Mato Grosso e Par&aacute; e possui capacidade de 1.820 MW. Obteve a Licen&ccedil;a de Opera&ccedil;&atilde;o N&ordm; 1272/2014 emitida pelo IBAMA em 19/11/2014.</p>\r\n\r\n<p>A Orienta Socioambiental desenvolve as atividades relacionadas &agrave; avalia&ccedil;&atilde;o dos impactos socioambientais ocorridos na fase de implanta&ccedil;&atilde;o e opera&ccedil;&atilde;o da UHE Teles Pires e a consultoria de apoio &agrave; gest&atilde;o dos programas do meio bi&oacute;tico, f&iacute;sico e do licenciamento ambiental da UHE Teles Pires.</p>\r\n', '2015-07-23 17:21:14', '2015-07-23 17:21:14'),
(2, 1, '20150723142211_rio.jpg', 'Rio PCH I - Grupo Neoenergia', '<p>A Rio PCH 1 &eacute; formada pelas Pequenas Centrais Hidrel&eacute;tricas (PCH) Pirapetinga (20 MW) e Pedra do Garraf&atilde;o (19 MW) em opera&ccedil;&atilde;o no trecho m&eacute;dio inferior do rio Itabapoana, na divisa dos estados do Rio de Janeiro e Esp&iacute;rito Santo. A Licen&ccedil;a de Opera&ccedil;&atilde;o N&ordm; 813/2009 foi emitida 12 de janeiro de 2009.&nbsp;</p>\r\n\r\n<p>A Orienta Socioambiental concluiu a atividade referente a an&aacute;lise do processo de licenciamento ambiental com a identifica&ccedil;&atilde;o dos riscos associados aos programas e condicionantes socioambientais do empreendimento e desenvolve atualmente a regulariza&ccedil;&atilde;o e gest&atilde;o do atendimento aos compromissos inerentes ao processo de licenciamento ambiental do empreendimento Rio PCH 1.</p>\r\n', '2015-07-23 17:22:11', '2015-07-23 17:22:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `googlemaps`, `twitter`, `instagram`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'contato@orientasocioambiental.com.br', '(11) 3034 4873', '<p>Rua Mourato Coelho, 957 &middot; Pinheiros</p>\r\n\r\n<p>S&atilde;o Paulo/SP &middot; 05417-011</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.247786209598!2d-46.690547599999995!3d-23.559542599999983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57a290cb459b%3A0x84271202de7f57ca!2sR.+Mourato+Coelho%2C+957+-+Pinheiros%2C+S%C3%A3o+Paulo+-+SP%2C+05417-011!5e0!3m2!1spt-BR!2sbr!4v1437661645558" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '#twitter', '#instagram', '#linkedin', '0000-00-00 00:00:00', '2015-07-23 17:27:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa`
--

CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada1` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada2` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada3` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `empresa`
--

INSERT INTO `empresa` (`id`, `titulo`, `texto`, `chamada1`, `chamada2`, `chamada3`, `created_at`, `updated_at`) VALUES
(1, 'Confiança, Comprometimento, Comunicação', '<p>A Orienta Socioambiental Eireli &ndash; ME foi fundada pela profissional Ma&iacute;ra Fonseca da Cunha ap&oacute;s ter adquirido uma ampla experi&ecirc;ncia no planejamento, execu&ccedil;&atilde;o, monitoramento e no controle dos prazos e custos associados aos aspectos e impactos socioambientais na implanta&ccedil;&atilde;o de projetos de infraestrutura de m&eacute;dio e grande porte no Brasil na &uacute;ltima d&eacute;cada.</p>\r\n\r\n<p>A inspira&ccedil;&atilde;o para a abertura da empresa em meio a tantas outras que prestam servi&ccedil;o de qualidade no mercado de meio ambiente foi a de possibilitar que a experi&ecirc;ncia adquirida, tanto t&eacute;cnica quando operacional, pudesse ser utilizada em diversos projetos que est&atilde;o associados ao licenciamento ambiental.</p>\r\n\r\n<p>O nome e s&iacute;mbolo escolhido para representar o conceito dessa nova empresa est&aacute; relacionado ao direcionamento e orienta&ccedil;&atilde;o de um caminho a ser trilhado em parceria com seu cliente e baseado na confian&ccedil;a, no comprometimento e na comunica&ccedil;&atilde;o, que s&atilde;o nossos valores, para a execu&ccedil;&atilde;o dos projetos em atendimento ao licenciamento ambiental e requisitos legais com qualidade, cumprimento dos prazos e associado a um custo justo.</p>\r\n', '<p>Os aspectos relacionados aos meios f&iacute;sico e bi&oacute;tico &eacute; representado pela <strong>Rosa dos Ventos</strong> que significa a unidade dos elementos do universo, ou seja, os aspectos ambientais da natureza e do meio ambiente.</p>\r\n\r\n<p>Est&atilde;o inclu&iacute;dos nas posi&ccedil;&otilde;es da <strong>Rosa dos Ventos</strong> os recursos h&iacute;dricos e a qualidade da &aacute;gua, a fauna terrestre e ictiofauna, a flora e recomposi&ccedil;&atilde;o florestal, os res&iacute;duos e efluentes, a qualidade do ar e monitoramentos clim&aacute;ticos, os processos erosivos, a geologia e geomorfologia, contamina&ccedil;&atilde;o do solo entre muitos outros processos ambientais.</p>\r\n', '<p>Os aspectos sociais, relacionados &agrave; legisla&ccedil;&atilde;o brasileira e internacional, &agrave;s popula&ccedil;&otilde;es atingidas e comunidades tradicionais, &agrave; educa&ccedil;&atilde;o ambiental, &agrave; preserva&ccedil;&atilde;o do patrim&ocirc;nio hist&oacute;rico, arqueol&oacute;gico, paleontol&oacute;gico, cultural e espeleol&oacute;gico e &agrave; interface com os &oacute;rg&atilde;os p&uacute;blicos, licenciadores e intervenientes, s&atilde;o representados pelo <strong>Homem</strong> que envolve e interage fortemente com a rosa dos ventos e os aspectos ambientais.</p>\r\n', '<p><strong>Dessa forma todos os aspectos relacionados ao meio ambiente est&atilde;o representados na Orienta Socioambiental que possui como vis&atilde;o ser uma empresa integradora dos meios f&iacute;sico, bi&oacute;tico, social e econ&ocirc;mico promovendo a gest&atilde;o socioambiental de empreendimentos complexos. Esse &eacute; o nosso neg&oacute;cio. Essa &eacute; a nossa miss&atilde;o.</strong></p>\r\n', '0000-00-00 00:00:00', '2015-07-23 17:10:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_07_22_162651_create_banners_table', 1),
('2015_07_22_162736_create_empresa_table', 1),
('2015_07_22_162750_create_atuacao_table', 1),
('2015_07_22_162811_create_projetos_table', 1),
('2015_07_22_162818_create_clientes_table', 1),
('2015_07_22_162835_create_parceiros_table', 1),
('2015_07_22_162843_create_contato_table', 1),
('2015_07_22_162900_create_contatos_recebidos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `parceiros`
--

CREATE TABLE IF NOT EXISTS `parceiros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `parceiros_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `parceiros`
--

INSERT INTO `parceiros` (`id`, `ordem`, `marca`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, '20150723142248_buriti.png', 'Buriti', 'buriti', '<p>A Buriti realiza diversas atividades ligadas &agrave; &aacute;rea socioambiental. &Eacute; especializada no desenvolvimento de pesquisas e estudos t&eacute;cnicos, planejamento e execu&ccedil;&atilde;o de programas de gest&atilde;o e educa&ccedil;&atilde;o ambiental, aliando ci&ecirc;ncia e tecnologia aos saberes locais, arte e cultura. Para n&oacute;s o di&aacute;logo com as comunidades &eacute; a chave para garantir o sucesso.</p>\r\n', '2015-07-23 17:22:48', '2015-07-23 17:22:48'),
(2, 0, '20150723142414_spelayon.png', 'Spelayon Consultoria', 'spelayon-consultoria', '<p>Com o nome derivado do grego, que significa caverna, a Spelayon Consultoria foi fundada para dar suporte t&eacute;cnico para os empreendimentos que pretendam ser licenciados em &aacute;reas que conflitem com o Patrim&ocirc;nio Espeleol&oacute;gico. Para isso conta com uma equipe multidisciplinar formada por profissionais das &aacute;reas de engenharia, geologia, espeleologia, geografia, biologia, ecologia e qu&iacute;mica. Executam seus servi&ccedil;os de acordo com a legisla&ccedil;&atilde;o ambiental com honestidade, &eacute;tica e transpar&ecirc;ncia.</p>\r\n', '2015-07-23 17:24:14', '2015-07-23 17:24:14'),
(3, 0, '20150723142432_ichthyology.png', 'Ichthyology - Consultoria Ambiental', 'ichthyology-consultoria-ambiental', '<p>A ICHTHYOLOGY Consultoria Ambiental LTDA, &eacute; uma empresa especializada em atividades referentes ao meio bi&oacute;tico de forma a propor solu&ccedil;&otilde;es voltadas a conciliar desenvolvimento com o menor dano poss&iacute;vel ao meio ambiente.</p>\r\n\r\n<p>A empresa foi fundada em 2005, com a finalidade de trabalhar na &aacute;rea de consultoria e licenciamento ambiental. Desde sua funda&ccedil;&atilde;o, muitas parcerias com profissionais de outras &aacute;reas foram criadas, tais como: Bi&oacute;logos das &aacute;reas de ictiologia, mastozoologia, herpetologia, ornitologia, limnologia, bentofauna, bot&acirc;nica, histologia, biogen&eacute;tica; Veterin&aacute;rios; Engenheiros Civil, Hidr&aacute;ulico, Florestal, Agr&ocirc;nomo; entre outros.</p>\r\n', '2015-07-23 17:24:32', '2015-07-23 17:24:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `projetos_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Planejamento', 'planejamento', '<p>Planejamento Ambiental do Desvio do Rio e do Enchimento do Reservat&oacute;rio da UHE Teles Pires (Parana&iacute;ta/MT).</p>\r\n\r\n<p>Elabora&ccedil;&atilde;o do Plano de Recupera&ccedil;&atilde;o de &Aacute;reas Degradadas do AHE Dardanelos (Aripuan&atilde;/MT).</p>\r\n\r\n<p>Sistema de Transposi&ccedil;&atilde;o de Peixes da UHE Teles Pires (Parana&iacute;ta/MT): interface junto ao projeto de engenharia e dos estudos ambientais para defini&ccedil;&atilde;o pela implanta&ccedil;&atilde;o ou n&atilde;o do mecanismo de transposi&ccedil;&atilde;o no corpo do barramento.</p>\r\n\r\n<p>Estudos do meio bi&oacute;tico para empresas de minera&ccedil;&atilde;o de pequeno e m&eacute;dio porte em Minas Gerais.</p>\r\n', '0000-00-00 00:00:00', '2015-07-23 17:20:42'),
(2, 'Execução', 'execucao', '<p>Coordena&ccedil;&atilde;o Ambiental do Plano Ambiental da Constru&ccedil;&atilde;o, do Plano de Recupera&ccedil;&atilde;o de &Aacute;reas Degradadas e Responsabilidade Social do AHE Dardanelos (Aripuan&atilde;/MT) pelo per&iacute;odo de 24 meses</p>\r\n\r\n<p>Coordena&ccedil;&atilde;o Ambiental do Plano Ambiental da Constru&ccedil;&atilde;o da UHE Santo Ant&ocirc;nio (Porto Velho/RO) pelo per&iacute;odo de 16 meses</p>\r\n\r\n<p>Coordena&ccedil;&atilde;o Ambiental dos Programas do Meio Bi&oacute;tico, F&iacute;sico, Educa&ccedil;&atilde;o Ambiental e do atendimento ao licenciamento Ambiental do Projeto de Desenvolvimento de Submarino (Itagua&iacute;/RJ) pelo per&iacute;odo de 04 meses</p>\r\n\r\n<p>Gerenciamento Ambiental dos Programas do Meio Bi&oacute;tico e F&iacute;sico e do licenciamento ambiental da UHE Teles Pires (Parana&iacute;ta/MT) pelo per&iacute;odo de 33 meses</p>\r\n', '0000-00-00 00:00:00', '2015-07-23 17:20:16'),
(3, 'Monitoramento e Controle', 'monitoramento-e-controle', '<p>Coordena&ccedil;&atilde;o Ambiental da implanta&ccedil;&atilde;o do Sistema de Gest&atilde;o Integrado do Cons&oacute;rcio Santo Ant&ocirc;nio Civil para a certifica&ccedil;&atilde;o externa da ISO 14001 e OHSAS 18001</p>\r\n\r\n<p>Participa&ccedil;&atilde;o da equipe de Invent&aacute;rio de Emiss&otilde;es de Gases de Efeito Estufa da UHE Santo Ant&ocirc;nio</p>\r\n\r\n<p>Implanta&ccedil;&atilde;o do Sistema de Gest&atilde;o da UHE Teles Pires em conjunto com equipe multidisciplinar</p>\r\n\r\n<p>Participa&ccedil;&atilde;o da equipe multidisciplinar de Atendimento aos Princ&iacute;pios do Equador e dos padr&otilde;es de desempenho do IFC - International Finance Corporation da UHE Santo Ant&ocirc;nio e UHE Teles Pires</p>\r\n\r\n<p>An&aacute;lise do processo de licenciamento ambiental com a identifica&ccedil;&atilde;o dos riscos associados aos programas e condicionantes socioambientais da Rio PCH 1</p>\r\n\r\n<p>Avalia&ccedil;&atilde;o dos impactos socioambientais ocorridos na fase de implanta&ccedil;&atilde;o e opera&ccedil;&atilde;o da UHE Teles Pires</p>\r\n', '0000-00-00 00:00:00', '2015-07-23 17:19:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', 'trupe', '$2y$10$1oBOpwocKlG6o5RiJglOOu1KU9LfXHts2wolNgftqSCarw6cFl4o.', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
