$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
        bootbox.confirm({
            size: 'small',
            backdrop: true,
            message: 'Deseja excluir o registro?',
            buttons: {
                'cancel': {
                    label: 'Cancelar',
                    className: 'btn-default btn-sm'
                },
                'confirm': {
                    label: '<span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir',
                    className: 'btn-primary btn-danger btn-sm'
                }
            },
            callback: function(result) {
                if (result) form.submit();
            }
        });
  	});

    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post(BASE + '/painel/ajax/order', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

    var fontSizesBanner = '';
    for (var i = 30; i <= 80; i++) {
        fontSizesBanner += i + '/' + i + 'px;';
    }

    var TEXTAREA_CONFIG = {
        padrao: {
            toolbar: [['Bold', 'Italic']]
        },

        banner: {
            fontSize_sizes: fontSizesBanner,
            toolbar: [['FontSize', 'Bold', 'Italic'], ['JustifyLeft', 'JustifyRight']]
        },

        clean: {
            toolbar: []
        }
    };

    $('.ckeditor').each(function (i, obj) {
        CKEDITOR.replace(obj.id, TEXTAREA_CONFIG[obj.dataset.editor]);
    });

});
