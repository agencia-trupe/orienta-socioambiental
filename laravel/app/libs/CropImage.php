<?php

use \Image, \Input;

class CropImage
{

    public static function make($input, $object)
    {
        if (!Input::hasFile($input) || !$object) {
            return false;
        }

        $image = Input::file($input);
        $name  = date('YmdHis').'_'.$image->getClientOriginalName();

        if (!is_array(array_values($object)[0])) $object = array($object);

        foreach($object as $data) {

            $path   = $data['path'].$name;
            $width  = $data['width'];
            $height = $data['height'];
            $upsize = (array_key_exists('upsize', $data) ? $data['upsize'] : false);

            $imgobj = Image::make($image->getRealPath());

            if ($width == null && $height == null) {
                $imgobj->save($path, 100);
            } elseif ($width == null || $height == null) {
                $imgobj->resize($width, $height, function($constraint) use ($upsize) {
                    $constraint->aspectRatio();
                    if ($upsize) { $constraint->upsize(); }
                })->save($path, 100);
            } else {
                $imgobj->fit($width, $height, function ($constraint) use ($upsize) {
                    if ($upsize) { $constraint->upsize(); }
                })->save($path, 100);
            }

            $imgobj->destroy();

        }

        return $name;
    }

}
