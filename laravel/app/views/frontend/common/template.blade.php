<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">

    <meta name="description" content="{{ Config::get('projeto.description') }}">
    <meta name="keywords" content="{{ Config::get('projeto.keywords') }}">
    <meta property="og:title" content="{{ Config::get('projeto.title') }}">
    <meta property="og:description" content="{{ Config::get('projeto.description') }}">
    <meta property="og:site_name" content="{{ Config::get('projeto.title') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:image" content="{{ Config::get('projeto.share_image') }}">

    <title>{{ Config::get('projeto.title') }}</title>

    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <link rel="icon" href="{{ url('favicon.ico') }}">

    <script>var BASE = '{{ url() }}';</script>
</head>
<body class="{{ Route::currentRouteName() }}">
    <div class="wrapper">
        @include('frontend.common.header')
        @yield('content')
        @include('frontend.common.footer')
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
    <script src="{{ asset('assets/js/jquery.cycle2.min.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
</body>
</html>
