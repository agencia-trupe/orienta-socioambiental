@extends('frontend.common.template')
@section('content')

    <main class="not-found">
        <div class="center">
            <h1>Página não encontrada</h1>
        </div>
    </main>

@stop