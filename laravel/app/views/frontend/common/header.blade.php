    <header>
        <div class="center">
            @if(isset($titulo))
            <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
            <h2>{{ $titulo }}</h2>
            @endif

            <a href="#" id="menu-toggle">
                <span class="lines"></span>
            </a>
            <nav>
                <a href="{{ route('empresa') }}" @if(str_is('empresa*', Route::currentRouteName())) class='active' @endif>Empresa</a>
                <a href="{{ route('atuacao') }}" @if(str_is('atuacao*', Route::currentRouteName())) class='active' @endif>Atuação</a>
                <a href="{{ route('projetos') }}" @if(str_is('projetos*', Route::currentRouteName())) class='active' @endif>Projetos</a>
                <a href="{{ route('clientes') }}" @if(str_is('clientes*', Route::currentRouteName())) class='active' @endif>Clientes</a>
                <a href="{{ route('parceiros') }}" @if(str_is('parceiros*', Route::currentRouteName())) class='active' @endif>Parceiros</a>
                <a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a>
                <div class="bg"></div>
            </nav>

            <ul class="social">
                @if($contato->twitter)
                <li><a href="{{ $contato->twitter }}" class="twitter" target="_blank">twitter</a></li>
                @endif
                @if($contato->instagram)
                <li><a href="{{ $contato->instagram }}" class="instagram" target="_blank">instagram</a></li>
                @endif
                @if($contato->linkedin)
                <li><a href="{{ $contato->linkedin }}" class="linkedin" target="_blank">linkedin</a></li>
                @endif
            </ul>
        </div>
    </header>
