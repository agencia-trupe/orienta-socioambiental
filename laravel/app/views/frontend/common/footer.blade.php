    <footer>
        <div class="center">
            <p class="left">
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a> <span>|</span> {{ $contato->telefone }}
            </p>
            <p class="right">
                © {{ date('Y') }} {{ Config::get('projeto.name') }} - Todos os direitos reservados.<br>
                <a href="http://trupe.net" target="_blank">Criação de sites:</a> <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
