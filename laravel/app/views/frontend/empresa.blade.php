@section('content')

    <div class="center">
        <div class="marca">
            <img src="{{ asset('assets/img/layout/marca-empresa.png') }}" alt="">
        </div>
        <div class="texto">
            <h3>{{ $empresa->titulo }}</h3>
            <div class="texto-principal">
                {{ $empresa->texto }}
            </div>
            <div class="texto-chamada">
                <img src="{{ asset('assets/img/layout/empresa-1.png') }}" alt="">
                {{ $empresa->chamada1 }}
            </div>
            <div class="texto-chamada">
                <img src="{{ asset('assets/img/layout/empresa-2.png') }}" alt="">
                {{ $empresa->chamada2 }}
            </div>
            <div class="texto-chamada">
                <img src="{{ asset('assets/img/layout/empresa-3.png') }}" alt="">
                {{ $empresa->chamada3 }}
            </div>
            <div class="texto-chamada texto-chamada-contato">
                <p>Estamos à seu dispor e esperamos seu <a href="{{ route('contato') }}">contato!</a></p>
            </div>
        </div>
    </div>

@stop
