@section('content')

    <div class="center">
        <form action="" id="form-contato">
            <input type="text" name="nome" id="nome" placeholder="Nome" required>
            <input type="email" name="email" id="email" placeholder="E-mail" required>
            <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
            <input type="submit" value="Enviar">
            <div id="form-resposta"></div>
        </form>

        <div class="info">
            <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
            <p class="telefone">{{ $contato->telefone }}</p>
            <div class="endereco">
                {{ $contato->endereco }}
            </div>
        </div>
    </div>

    <div class="googlemaps">
        {{ $contato->googlemaps }}
    </div>

@stop
