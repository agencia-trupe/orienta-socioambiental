@section('content')

    <div class="center">
        @foreach($clientes as $cliente)
        <div class="cliente-row">
            <div class="marca">
                <img src="{{ asset('assets/img/clientes/'.$cliente->marca) }}" alt="">
            </div>
            <div class="texto">
                <h3>{{ $cliente->titulo }}</h3>
                {{ $cliente->texto }}
            </div>
        </div>
        @endforeach
    </div>

@stop
