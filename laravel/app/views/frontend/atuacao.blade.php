@section('content')

    <div class="center">
        <div id="pager"></div>
        <div class="atuacao-slides">
            @foreach($atuacao as $slide)
            <div class="slide" data-cycle-hash="{{ $slide->slug }}">
                <h3 class="{{ $slide->slug }}">{{ $slide->titulo }}</h3>
                {{ $slide->texto }}
                <a href="{{ route('projetos').'#'.$slide->slug }}">
                    <span>Veja nossos projetos</span>
                </a>
            </div>
            @endforeach
            <a href="#" class="cycle-arrow cycle-prev">prev</a>
            <a href="#" class="cycle-arrow cycle-next">next</a>
        </div>
    </div>

@stop
