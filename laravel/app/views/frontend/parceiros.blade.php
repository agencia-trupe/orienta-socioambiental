@section('content')

    <div class="center">
        <div id="pager"></div>
        <div class="parceiros-slides">
            @foreach($parceiros as $slide)
            <div class="slide" data-cycle-hash="{{ $slide->slug }}">
                <div class="imagem">
                    <img src="{{ asset('assets/img/parceiros/'.$slide->marca) }}" alt="">
                </div>
                <h3 class="{{ $slide->slug }}">{{ $slide->titulo }}</h3>
                {{ $slide->texto }}
            </div>
            @endforeach
            <a href="#" class="cycle-arrow cycle-prev">prev</a>
            <a href="#" class="cycle-arrow cycle-next">next</a>
        </div>
    </div>

@stop
