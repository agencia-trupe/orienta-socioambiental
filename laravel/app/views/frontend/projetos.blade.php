@section('content')

    <div class="center">
        <div id="pager"></div>
        <div class="projetos-slides">
            @foreach($projetos as $slide)
            <div class="slide" data-titulo="{{ $slide->titulo }}" data-cycle-hash="{{ $slide->slug }}">
                {{ $slide->texto }}
            </div>
            @endforeach
        </div>
    </div>

@stop
