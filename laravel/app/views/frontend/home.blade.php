@section('content')

    <div class="banners">
        <div class="banners-slides">
            @foreach($banners as $banner)
            <div class="slide" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                <div class="center">
                    {{ $banner->texto }}
                </div>
            </div>
            @endforeach
        </div>
        <div class="center">
            <div class="logo"></div>
            <div id="pager"></div>
        </div>
    </div>

@stop
