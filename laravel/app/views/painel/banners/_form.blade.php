@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="well form-group">
    {{ Form::label('imagem', 'Imagem') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$banner->imagem) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'banner']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
