@section('content')

    <legend>
        <h2><small>Projetos /</small> {{ $pagina->titulo }}</h2>
    </legend>

    {{ Form::model($pagina, [
        'route' => ['painel.projetos.update', $pagina->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.projetos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
