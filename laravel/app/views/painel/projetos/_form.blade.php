@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.projetos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
