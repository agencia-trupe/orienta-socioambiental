@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="well form-group">
    {{ Form::label('marca', 'Marca') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/clientes/'.$cliente->marca) }}" style="display:block; margin-bottom: 10px; max-width:210px;height:auto;">
@endif
    {{ Form::file('marca', ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.clientes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
