@section('content')

    <legend>
        <h2>Adicionar Cliente</h2>
    </legend>

    {{ Form::open(['route' => 'painel.clientes.store', 'files' => true]) }}

        @include('painel.clientes._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
