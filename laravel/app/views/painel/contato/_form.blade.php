@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('email', 'E-mail') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('telefone', 'Telefone') }}
    {{ Form::text('telefone', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('endereco', 'Endereço') }}
    {{ Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) }}
</div>

<div class="form-group">
    {{ Form::label('googlemaps', 'Código Google Maps') }}
    {{ Form::text('googlemaps', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('twitter', 'Twitter') }}
    {{ Form::text('twitter', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('instagram', 'Instagram') }}
    {{ Form::text('instagram', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('linkedin', 'Linkedin') }}
    {{ Form::text('linkedin', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}
