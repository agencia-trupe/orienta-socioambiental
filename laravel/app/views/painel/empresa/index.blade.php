@section('content')

    <legend>
        <h2>Empresa</h2>
    </legend>

    {{ Form::model($empresa, [
        'route' => ['painel.empresa.update', $empresa->id],
        'method' => 'patch'])
    }}

        @include('painel.empresa._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
