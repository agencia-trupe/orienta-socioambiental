@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
</div>

<hr>

<div class="row">
    <div class="col-sm-2">
        <img src="{{ asset('assets/img/layout/empresa-1.png') }}" class="img-responsive" style="margin:0 auto;" alt="">
    </div>

    <div class="form-group col-sm-10">
        {{ Form::label('chamada1', 'Chamada 1') }}
        {{ Form::textarea('chamada1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
    </div>
</div>

<hr>

<div class="row">
    <div class="col-sm-2">
        <img src="{{ asset('assets/img/layout/empresa-2.png') }}" class="img-responsive" style="margin:0 auto;" alt="">
    </div>

    <div class="form-group col-sm-10">
        {{ Form::label('chamada2', 'Chamada 2') }}
        {{ Form::textarea('chamada2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
    </div>
</div>

<hr>

<div class="row">
    <div class="col-sm-2">
        <img src="{{ asset('assets/img/layout/empresa-3.png') }}" class="img-responsive" style="margin:0 auto;" alt="">
    </div>

    <div class="form-group col-sm-10">
        {{ Form::label('chamada3', 'Chamada 3') }}
        {{ Form::textarea('chamada3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) }}
    </div>
</div>

<hr>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}
