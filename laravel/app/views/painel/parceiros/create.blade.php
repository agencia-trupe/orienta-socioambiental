@section('content')

    <legend>
        <h2>Adicionar Parceiro</h2>
    </legend>

    {{ Form::open(['route' => 'painel.parceiros.store', 'files' => true]) }}

        @include('painel.parceiros._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
