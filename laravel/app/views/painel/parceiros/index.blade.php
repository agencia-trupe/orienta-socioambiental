@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Parceiros
            <a href="{{ route('painel.parceiros.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Parceiro</a>
        </h2>
    </legend>

    @if(count($parceiros))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="parceiros">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Marca</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($parceiros as $parceiro)

            <tr class="tr-row" id="id_{{ $parceiro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ url('assets/img/parceiros/'.$parceiro->marca) }}" alt="" style="width:100%;max-width:100px;height:auto;background-color:#937493;"></td>
                <td>{{ $parceiro->titulo }}</td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.parceiros.destroy', $parceiro->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.parceiros.edit', $parceiro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhum parceiro cadastrado.</div>
    @endif

@stop
