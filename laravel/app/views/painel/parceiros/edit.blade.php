@section('content')

    <legend>
        <h2>Editar Parceiro</h2>
    </legend>

    {{ Form::model($parceiro, [
        'route' => ['painel.parceiros.update', $parceiro->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.parceiros._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
