<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url() }}">{{ Config::get('projeto.name') }}</a>
        </div>

    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.banners.index') }}">Banners</a>
            </li>

            <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.empresa.index') }}">Empresa</a>
            </li>

            <li @if(str_is('painel.atuacao*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.atuacao.index') }}">Atuação</a>
            </li>

            <li @if(str_is('painel.projetos*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.projetos.index') }}">Projetos</a>
            </li>

            <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.clientes.index') }}">Clientes</a>
            </li>

            <li @if(str_is('painel.parceiros*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.parceiros.index') }}">Parceiros</a>
            </li>

            <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contato @if(isset($recebidos_count) && $recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
                    <li><a href="{{ route('painel.contato.recebidos.index') }}">Contatos Recebidos @if(isset($recebidos_count) && $recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif</a></li>
                </ul>
            </li>

            <li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.usuarios.index') }}">Usuários</a></li>
                    <li><a href="{{ route('painel.logout') }}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    </div>
</nav>
