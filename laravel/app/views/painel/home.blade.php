@section('content')

    <h1>Bem Vindo(a),</h1>
    <br>
    <p>Pelo painel administrativo da trupe você pode controlar as principais funções e conteúdo do site.</p>
    <p>Em caso de dúvidas entrar em contato: <a href="mailto:contato@trupe.net">contato@trupe.net</a></p>

@stop