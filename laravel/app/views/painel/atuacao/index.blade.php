@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Atuação
        </h2>
    </legend>

    @if(count($paginas))
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Página</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($paginas as $pagina)

            <tr class="tr-row">

                <td>{{ $pagina->titulo }}</td>
                <td class="crud-actions">
                    <a href="{{ route('painel.atuacao.edit', $pagina->id ) }}" class="btn btn-primary btn-sm pull-left"><span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar</a>
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma página cadastrada.</div>
    @endif

@stop
