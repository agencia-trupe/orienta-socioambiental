@section('content')

    <legend>
        <h2><small>Atuação /</small> {{ $pagina->titulo }}</h2>
    </legend>

    {{ Form::model($pagina, [
        'route' => ['painel.atuacao.update', $pagina->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.atuacao._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
