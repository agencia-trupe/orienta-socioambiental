<?php

return array(

    'name'        => 'Orienta Socioambiental',
    'title'       => 'Orienta Socioambiental',
    'description' => '',
    'keywords'    => 'consultoria ambiental, empresa de meio ambiente, gestão ambiental, monitoramento ambiental, supervisão de meio ambiente, estudos socioambientais, avaliação ambiental, prestação de serviço meio ambiente, atendimento legislação ambiental, prestação serviço ambiental, plano de manejo, empresa direito ambiental, auditoria ambiental, empresa princípios do equador, atendimento princípios do equador',
    'share_image' => asset('assets/img/layout/marca-empresa.png')

);
