<?php

class ProjetosController extends BaseController {

	public function index()
	{
        View::share('titulo', 'Projetos');

        $projetos = Projeto::get();

		return $this->view('frontend.projetos', compact('projetos'));
	}

}
