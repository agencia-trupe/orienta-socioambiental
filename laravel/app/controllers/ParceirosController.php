<?php

class ParceirosController extends BaseController {

	public function index()
	{
        View::share('titulo', 'Parceiros');

        $parceiros = Parceiro::ordenados()->get();

		return $this->view('frontend.parceiros', compact('parceiros'));
	}

}
