<?php

class ClientesController extends BaseController {

	public function index()
	{
        View::share('titulo', 'Clientes');

        $clientes = Cliente::ordenados()->get();

		return $this->view('frontend.clientes', compact('clientes'));
	}

}
