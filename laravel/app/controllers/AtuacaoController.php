<?php

class AtuacaoController extends BaseController {

	public function index()
	{
        View::share('titulo', 'Atuação');

        $atuacao = Atuacao::get();

		return $this->view('frontend.atuacao', compact('atuacao'));
	}

}
