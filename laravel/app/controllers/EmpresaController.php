<?php

class EmpresaController extends BaseController {

	public function index()
	{
        View::share('titulo', 'Empresa');

        $empresa = Empresa::first();

		return $this->view('frontend.empresa', compact('empresa'));
	}

}
