<?php

class ContatoController extends BaseController {

	public function index()
	{
        View::share('titulo', 'Contato');

		return $this->view('frontend.contato');
	}

    public function envio()
    {
        $contato = Contato::first();

        $nome = Input::get('nome');
        $email = Input::get('email');
        $mensagem = Input::get('mensagem');

        $validation = Validator::make(
            array(
                'nome'     => $nome,
                'email'    => $email,
                'mensagem' => $mensagem
            ),
            array(
                'nome'     => 'required',
                'email'    => 'required|email',
                'mensagem' => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail',
                'message' => 'Preencha todos os campos corretamente.'
            );
            return Response::json($response);
        }

        if (isset($contato->email))
        {
            $data = array(
                'nome'     => $nome,
                'email'    => $email,
                'mensagem' => $mensagem
            );

            Mail::send('emails.contato', $data, function($message) use ($data, $contato)
            {
                $message->to($contato->email, Config::get('projeto.name'))
                        ->subject('[CONTATO] '.Config::get('projeto.name'))
                        ->replyTo($data['email'], $data['nome']);
            });
        }

        $object = new ContatoRecebido;
        $object->nome = $nome;
        $object->email = $email;
        $object->mensagem = $mensagem;
        $object->save();

        $response = array(
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        );
        return Response::json($response);
    }

}
