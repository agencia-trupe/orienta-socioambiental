<?php

namespace Painel;

use \Cliente, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class ClientesController extends BasePainelController {

    private $validation_rules = [
        'marca'  => 'required|image',
        'titulo' => 'required',
        'texto'  => 'required'
    ];

    private $image_config = [
        'width'  => 210,
        'height' => 210,
        'path'   => 'assets/img/clientes/'
    ];

    public function index()
    {
        $clientes = Cliente::ordenados()->get();

        return $this->view('painel.clientes.index', compact('clientes'));
    }

    public function create()
    {
        return $this->view('painel.clientes.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['marca'] = CropImage::make('marca', $this->image_config);
            Cliente::create($input);
            Session::flash('sucesso', 'Cliente adicionado com sucesso.');

            return Redirect::route('painel.clientes.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao adicionar cliente.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);

        return $this->view('painel.clientes.edit', compact('cliente'));
    }

    public function update($id)
    {
        $cliente = Cliente::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['marca'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('marca')) {
                $input['marca'] = CropImage::make('marca', $this->image_config);
            } else {
                unset($input['marca']);
            }

            $cliente->update($input);
            Session::flash('sucesso', 'Cliente alterado com sucesso.');

            return Redirect::route('painel.clientes.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar cliente.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            cliente::destroy($id);
            Session::flash('sucesso', 'Cliente removido com sucesso.');

            return Redirect::route('painel.clientes.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover cliente.']);

        }
    }

}
