<?php

namespace Painel;

use \Empresa, \Input, \Session, \Redirect, \Validator;

class EmpresaController extends BasePainelController {

    private $validation_rules = [
        'titulo'   => 'required',
        'texto'    => 'required',
        'chamada1' => 'required',
        'chamada2' => 'required',
        'chamada3' => 'required'
    ];

    public function index()
    {
    	$empresa = Empresa::first();

        return $this->view('painel.empresa.index', compact('empresa'));
    }

    public function update($id)
    {
        $empresa = Empresa::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $empresa->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.empresa.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
