<?php

namespace Painel;

use \Parceiro, \View, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class ParceirosController extends BasePainelController {

    private $validation_rules = [
        'marca'  => 'required|image',
        'titulo' => 'required',
        'texto'  => 'required'
    ];

    private $image_config = [
        'width'  => 260,
        'height' => 260,
        'path'   => 'assets/img/parceiros/'
    ];

    public function index()
    {
        $parceiros = Parceiro::ordenados()->get();

        return $this->view('painel.parceiros.index', compact('parceiros'));
    }

    public function create()
    {
        return $this->view('painel.parceiros.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug']  = Str::slug(Input::get('titulo'));
            $input['marca'] = CropImage::make('marca', $this->image_config);
            Parceiro::create($input);
            Session::flash('sucesso', 'Parceiro adicionado com sucesso.');

            return Redirect::route('painel.parceiros.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao adicionar parceiro. Verifique se já existe outro cadastro com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $parceiro = Parceiro::findOrFail($id);

        return $this->view('painel.parceiros.edit', compact('parceiro'));
    }

    public function update($id)
    {
        $parceiro = Parceiro::findOrFail($id);
        $input    = Input::all();

        $this->validation_rules['marca'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('marca')) {
                $input['marca'] = CropImage::make('marca', $this->image_config);
            } else {
                unset($input['marca']);
            }

            $parceiro->update($input);
            Session::flash('sucesso', 'Parceiro alterado com sucesso.');

            return Redirect::route('painel.parceiros.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao editar parceiro. Verifique se já existe outro cadastro com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Parceiro::destroy($id);
            Session::flash('sucesso', 'Parceiro removido com sucesso.');

            return Redirect::route('painel.parceiros.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover parceiro.']);

        }
    }

}
