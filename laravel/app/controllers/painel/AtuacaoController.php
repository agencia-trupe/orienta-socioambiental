<?php

namespace Painel;

use \Atuacao, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class AtuacaoController extends BasePainelController {

    private $validation_rules = [
        'texto'  => 'required'
    ];

    public function index()
    {
        $paginas = Atuacao::get();

        return $this->view('painel.atuacao.index', compact('paginas'));
    }

    public function edit($id)
    {
        $pagina = Atuacao::findOrFail($id);

        return $this->view('painel.atuacao.edit', compact('pagina'));
    }

    public function update($id)
    {
        $pagina = Atuacao::findOrFail($id);
        $input  = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $pagina->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.atuacao.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
