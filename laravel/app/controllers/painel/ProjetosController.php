<?php

namespace Painel;

use \Projeto, \View, \Input, \Session, \Redirect, \Validator, \CropImage;

class ProjetosController extends BasePainelController {

    private $validation_rules = [
        'texto'  => 'required'
    ];

    public function index()
    {
        $paginas = Projeto::get();

        return $this->view('painel.projetos.index', compact('paginas'));
    }

    public function edit($id)
    {
        $pagina = Projeto::findOrFail($id);

        return $this->view('painel.projetos.edit', compact('pagina'));
    }

    public function update($id)
    {
        $pagina = Projeto::findOrFail($id);
        $input  = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $pagina->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.projetos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}
