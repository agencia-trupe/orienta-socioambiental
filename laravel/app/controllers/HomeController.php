<?php

class HomeController extends BaseController {

	public function index()
	{
        $banners = Banner::ordenados()->get();

		return $this->view('frontend.home', compact('banners'));
	}

}
