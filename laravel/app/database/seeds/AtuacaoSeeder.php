<?php

class AtuacaoSeeder extends Seeder {

    public function run()
    {
        DB::table('atuacao')->delete();

        $data = array(
            array(
                'titulo' => 'Planejamento',
                'slug'   => 'planejamento',
                'texto'  => ''
            ),
            array(
                'titulo' => 'Execução',
                'slug'   => 'execucao',
                'texto'  => ''
            ),
            array(
                'titulo' => 'Monitoramento e Controle',
                'slug'   => 'monitoramento-e-controle',
                'texto'  => ''
            )
        );

        DB::table('atuacao')->insert($data);
    }

}
