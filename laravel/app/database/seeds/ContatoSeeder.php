<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'email'      => '',
                'telefone'   => '',
                'endereco'   => '',
                'googlemaps' => '',
                'twitter'    => '',
                'instagram'  => '',
                'linkedin'   => ''
            )
        );

        DB::table('contato')->insert($data);
    }

}
