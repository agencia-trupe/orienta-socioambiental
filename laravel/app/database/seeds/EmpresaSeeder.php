<?php

class EmpresaSeeder extends Seeder {

    public function run()
    {
        DB::table('empresa')->delete();

        $data = array(
            array(
                'titulo'   => '',
                'texto'    => '',
                'chamada1' => '',
                'chamada2' => '',
                'chamada3' => '',
            )
        );

        DB::table('empresa')->insert($data);
    }

}
