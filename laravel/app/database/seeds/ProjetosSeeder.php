<?php

class ProjetosSeeder extends Seeder {

    public function run()
    {
        DB::table('projetos')->delete();

        $data = array(
            array(
                'titulo' => 'Planejamento',
                'slug'   => 'planejamento',
                'texto'  => ''
            ),
            array(
                'titulo' => 'Execução',
                'slug'   => 'execucao',
                'texto'  => ''
            ),
            array(
                'titulo' => 'Monitoramento e Controle',
                'slug'   => 'monitoramento-e-controle',
                'texto'  => ''
            )
        );

        DB::table('projetos')->insert($data);
    }

}
