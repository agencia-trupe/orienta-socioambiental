<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsuariosSeeder');
		$this->call('EmpresaSeeder');
		$this->call('ContatoSeeder');
		$this->call('AtuacaoSeeder');
		$this->call('ProjetosSeeder');
	}

}
