<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParceirosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('parceiros', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ordem')->default(0);
			$table->string('marca');
			$table->string('titulo');
			$table->string('slug')->unique();
			$table->text('texto');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('parceiros');
	}

}
