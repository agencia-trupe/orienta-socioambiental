<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('empresa', ['as' => 'empresa', 'uses' => 'EmpresaController@index']);
Route::get('atuacao', ['as' => 'atuacao', 'uses' => 'AtuacaoController@index']);
Route::get('projetos', ['as' => 'projetos', 'uses' => 'ProjetosController@index']);
Route::get('clientes', ['as' => 'clientes', 'uses' => 'ClientesController@index']);
Route::get('parceiros', ['as' => 'parceiros', 'uses' => 'ParceirosController@index']);
Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Painel

Route::get('painel', [
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
]);

Route::get('painel/login', [
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
]);
Route::post('painel/login', [
    'as'   => 'painel.auth',
    'uses' => 'Painel\HomeController@attempt'
]);
Route::get('painel/logout', [
    'as'   => 'painel.logout',
    'uses' => 'Painel\HomeController@logout'
]);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::resource('banners', 'Painel\BannersController');
    Route::resource('empresa', 'Painel\EmpresaController');
    Route::resource('atuacao', 'Painel\AtuacaoController');
    Route::resource('projetos', 'Painel\ProjetosController');
    Route::resource('clientes', 'Painel\ClientesController');
    Route::resource('parceiros', 'Painel\ParceirosController');
    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');
    Route::resource('usuarios', 'Painel\UsuariosController');

    Route::post('ajax/order', 'Painel\AjaxController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});
