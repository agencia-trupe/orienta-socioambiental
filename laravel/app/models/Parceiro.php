<?php

class Parceiro extends Eloquent
{

    protected $table = 'parceiros';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

}
