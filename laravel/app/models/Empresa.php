<?php

class Empresa extends Eloquent
{

    protected $table = 'empresa';

    protected $hidden = [];

    protected $guarded = ['id'];

}
