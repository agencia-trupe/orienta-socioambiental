<?php

class Projeto extends Eloquent
{

    protected $table = 'projetos';

    protected $hidden = [];

    protected $guarded = ['id'];

}
