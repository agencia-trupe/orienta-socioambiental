(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.menuToggle = function() {
        var $handle = $('#menu-toggle');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();

            $(this).next('nav').css({ height : $(document).height() });
            $(this).toggleClass('open');
        });

        $(window).on('resize', function() {
            $handle.next('nav').css({ height : $(document).height() });
        });

        $(window).on('click', function(event) {
            var outsideNav  = !$(event.target).closest('header').length,
                navIsActive = $handle.hasClass('open');

            if (outsideNav && navIsActive) {
                $handle.removeClass('open');
            }

            event.stopPropagation();
        });
    };

    App.bannersHome = function() {
        var $wrapper = $('.banners-slides');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.slide',
            pager: '#pager',
            pagerTemplate: '<a href=#>{{slideNum}}</a>'
        });
    };

    App.slidesAtuacao = function() {
        var $wrapper = $('.atuacao-slides');
        if (!$wrapper.length) return;

        $wrapper.find('.slide').css({visibility:'hidden'});

        $wrapper.cycle({
            slides: '>.slide',
            timeout: 0,
            pager: '#pager',
            pagerTemplate: '<a href=#>{{slideNum}}</a>',
            autoHeight: 'calc'
        });
    };

    App.slidesProjetos = function() {
        var $wrapper = $('.projetos-slides');
        if (!$wrapper.length) return;

        $wrapper.find('.slide').css({visibility:'hidden'});

        $wrapper.cycle({
            slides: '>.slide',
            timeout: 0,
            pager: '#pager',
            pagerTemplate: '<a href=# class={{hash}}><span>{{titulo}}</span></a>',
            autoHeight: 'calc'
        });
    };

    App.slidesParceiros = function() {
        var $wrapper = $('.parceiros-slides');
        if (!$wrapper.length) return;

        $wrapper.find('.slide').css({visibility:'hidden'});

        $wrapper.cycle({
            slides: '>.slide',
            timeout: 0,
            pager: '#pager',
            pagerTemplate: '<a href=#>{{slideNum}}</a>',
            autoHeight: 'calc'
        });
    };

    App.envioContato = function() {
        $('#form-contato').on('submit', function(event) {
            event.preventDefault();

            var $formContato = $(this);
            var $formContatoSubmit = $formContato.find('input[type=submit]');
            var $formContatoResposta = $formContato.find('#form-resposta');

            $formContatoResposta.hide();

            $.post(BASE + '/contato', {

                nome     : $('#nome').val(),
                email    : $('#email').val(),
                mensagem : $('#mensagem').val()

            }, function(data) {

                if (data.status == 'success') $formContato[0].reset();
                $formContatoResposta.hide().text(data.message).fadeIn('slow');

            }, 'json');
        });
    };

    App.bannersHomeResponsive = function() {
        $('.slide span').each(function(index, el) {
            $(el).attr('data-size', parseInt($(el).css('fontSize')));
        });

        var resizeFonts = function() {
            $('.slide span').each(function(index, el) {
                var $el = $(el),
                    windowWidth = $(window).width();

                if (windowWidth >= 768) {
                    $el.css('fontSize', $el.data('size'));
                } else {
                    $el.css('fontSize', function() {
                        var proportion = (windowWidth / 700),
                            fontsize   = $el.data('size'),
                            size       = fontsize * proportion;

                        return (size >= 16 ? size : 16);
                    });
                }
            });
        }
        $(window).resize(resizeFonts).trigger('resize');
    };

    App.init = function() {
        this.menuToggle();
        this.bannersHome();
        this.slidesAtuacao();
        this.slidesProjetos();
        this.slidesParceiros();
        this.envioContato();
        this.bannersHomeResponsive();
    };

    $(document).ready(function() {
        App.init();
    });

}(window, document, jQuery));
